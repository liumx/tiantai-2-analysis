import * as types from '../mutations-type'

const travel = {
  state: {
    searchContent: ''
  },

  getters: {
    getSearchContent (state) {
      return state.searchContent
    }
  },
  setters: {},
  mutations: {
    [types.SEARCH_CONTENT] (state, data) {
      state.searchContent = data
    }
  },
  actions: {
  }
}

export default travel
