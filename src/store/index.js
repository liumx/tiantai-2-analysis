import vue from 'vue'
import vuex from 'vuex'
import travel from './modules/travel'

vue.use(vuex)

export default new vuex.Store({
  modules: {
    travel
  }
})
