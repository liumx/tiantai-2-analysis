import axios from '@/http'
import {getUrl} from './path'

const media = {
  getNegative () {
    return axios({
      method: 'get',
      url: getUrl('/report/negative')
    })
  },
  getMixed (time) {
    return axios({
      method: 'get',
      url: getUrl(`/media/monitoring/${time}`)
    })
  }
}

export default media
