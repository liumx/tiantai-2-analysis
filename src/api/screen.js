import axios from '@/http'
import {getUrl} from './path'

const screen = {
  getImpress () { // 柯桥印象
    return axios({
      method: 'get',
      url: getUrl('/hotword/positive')
    })
  },
  getComplainHanding () { // 投诉纠纷
    return axios({
      method: 'get',
      url: getUrl('/dispute/type')
    })
  },
  getAllComplain () { // 投诉纠纷(右)
    return axios({
      method: 'get',
      url: getUrl('/dispute/statistics')
    })
  },
  getkeqiaoHigWay () {
    return axios({
      method: 'get',
      url: getUrl('/highway/keqiao')
    })
  },
  getHighSpeed (day) { // 高速路口车辆
    return axios({
      method: 'get',
      url: getUrl(`/highway/${day}`)
    })
  },
  getIncome (type) {
    return axios({
      method: 'get',
      url: getUrl(`/${type}/income`)
    })
  },
  getPassenger (type) {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/${type}`)
    })
  },
  getTrain (type) {
    return axios({
      method: 'get',
      url: getUrl(`/train/${type}`)
    })
  },
  getCountrytourPassenger () {
    return axios({
      method: 'get',
      url: getUrl(`/countrytour/statistics`)
    })
  },
  getNews () {
    return axios({
      method: 'get',
      url: getUrl(`/news`)
    })
  },
  getTodaySentiment () {
    return axios({
      methods: 'get',
      url: getUrl(`sentiment/today`)
    })
  },
  getHotData (type) {
    return axios({
      method: 'get',
      url: getUrl(`/hottest/${type}`)
    })
  },
  getAllPassenger () {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/total/month`)
    })
  },
  getPassengerRealtime () {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/realtime`)
    })
  }
}

export default screen
