import axios from '@/http'
import {getUrl} from './path'

const forecast = {
  getFuture () {
    return axios({
      method: 'get',
      url: getUrl('/passengerflow/forecast/universe')
    })
  },
  getScenicFuture () {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/forecast/scenic`)
    })
  }
}

export default forecast
