import axios from '@/http'
import {getUrl} from './path'

const consume = {
  getCateringConsume (day) {
    return axios({
      method: 'get',
      url: getUrl(`/consume/catering?month=${day}`)
    })
  },
  getHotelConsume (day) {
    return axios({
      method: 'get',
      url: getUrl(`/consume/hotel?month=${day}`)
    })
  },
  getScenicConsume (day) {
    return axios({
      method: 'get',
      url: getUrl(`/consume/scenic?month=${day}`)
    })
  },
  getCaterRatio (day) {
    return axios({
      method: 'get',
      url: getUrl(`/ratio/catering?month=${day}`)
    })
  },
  getHotelRatio (day) {
    return axios({
      method: 'get',
      url: getUrl(`/ratio/hotel?month=${day}`)
    })
  },
  getScenicRatio (day) {
    return axios({
      method: 'get',
      url: getUrl(`/ratio/scenic?month=${day}`)
    })
  },
  getAllConsume () {
    return axios({
      method: 'get',
      url: getUrl(`/consume/statistics`)
    })
  },
  getconsumeAnalysis (type) {
    return axios({
      method: 'get',
      url: getUrl(`/consume/analysis/${type}`)
    })
  },
  getConsumeDistri (month) {
    return axios({
      method: 'get',
      url: getUrl(`/ratio/statistics?month=${month}`)
    })
  },
  getAddressPasengerAna (type) {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/source/${type}?date=year`)
    })
  },
  getConsumeRanking (type) {
    return axios({
      method: 'get',
      url: getUrl(`/ranking/consume/${type}`)
    })
  }
}

export default consume
