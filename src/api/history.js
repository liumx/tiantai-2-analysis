import axios from '@/http'
import {getUrl} from './path'

const history = {
  assengerMax () { // 柯桥印象
    return axios({
      method: 'get',
      url: getUrl('/passengerflow/max')
    })
  },
  passengerflow (type) {
    return axios({
      method: 'get',
      url: getUrl(`passengerflow/total/${type}`)

    })
  },
  flowProportion () {
    return axios({
      method: 'get',
      url: getUrl('passengerflow/scenic')
    })
  },
  getHistoryPassenger (scenic, type) {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/history/${scenic}/${type}`)
    })
  },
  exportData (scenic, type) {
    return axios({
      method: 'get',
      url: getUrl(`passengerflow/export/${scenic}/${type}`)
    })
  },
  getAdvice () {
    return axios({
      method: 'get',
      url: getUrl(`/advice/diagnostic`)
    })
  },
  importData (params) {
    return axios({
      method: 'post',
      url: getUrl(`/passengerflow/import/month`),
      processData: false,
      contentType: false,
      data: params
    })
  }
}

export default history
