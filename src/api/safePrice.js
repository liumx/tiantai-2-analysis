import axios from '@/http'
import {getUrl} from './path'

const safePrice = {
  getUnusual () {
    return axios({
      method: 'get',
      url: getUrl('/price/anomaly')
    })
  },
  getYearPrice (source, type) {
    return axios({
      method: 'get',
      url: getUrl(`/averageprice/year/${source}/${type}`)
    })
  },
  getDayPrice (source, type) {
    return axios({
      method: 'get',
      url: getUrl(`/averageprice/day/${source}/${type}`)
    })
  }
}

export default safePrice
