import axios from '@/http'
import {getUrl} from './path'

const police = {
  getTotalYear (time) {
    return axios({
      method: 'get',
      url: getUrl(`/alarm/total/${time}`)
    })
  },
  getAllType (num) {
    return axios({
      methods: 'get',
      url: getUrl(`/alarm/date?month=${num}`)
    })
  },
  getPolice () {
    return axios({
      method: 'get',
      url: getUrl('/distribute/alarm')
    })
  }
}

export default police
