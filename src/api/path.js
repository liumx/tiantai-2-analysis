const baseUrlContentRoot = 'ttsv2'
function getUrl (url) {
  return `/${baseUrlContentRoot}${url}`
}

export { getUrl }
