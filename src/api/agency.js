import axios from '@/http'
import {getUrl} from './path'

const agency = {
  getTravelAgency (type) {
    return axios({
      method: 'get',
      url: getUrl(`/corporate/ranking/${type}`)
    })
  },
  getDimensionRanking (type) {
    return axios({
      method: 'get',
      url: getUrl(`/dimensionality/statistics?ranking=${type}`)
    })
  }
}

export default agency
