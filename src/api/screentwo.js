import axios from '@/http'
import {getUrl} from './path'

const screentwo = {
  getTrivelPAssenger () { // 柯桥印象
    return axios({
      method: 'get',
      url: getUrl('/passengerflow/reception')
    })
  },
  getPassengerAddress (type, data) {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/source/${type}?date=${data}`)
    })
  },
  getPassengerAdsNum (type, data) {
    return axios({
      method: 'get',
      url: getUrl(`/passengerflow/source/${type}?date=month&month=${data}`)
    })
  },
  getDispersionRatio (type) {
    return axios({
      method: 'get',
      url: getUrl(`/ticket/${type}`)
    })
  },
  getWaring () {
    return axios({
      method: 'get',
      url: getUrl('/warning')
    })
  },
  getpassengerForecast () {
    return axios({
      method: 'get',
      url: getUrl('/passengerflow/forecast/universe')
    })
  },
  getHotelPerson () {
    return axios({
      method: 'get',
      url: getUrl(`/hotel/yesterday`)
    })
  },
  getStayTime (type, data) {
    return axios({
      method: 'get',
      url: getUrl(`/hotel/${type}?date=${data}`)
    })
  }
}

export default screentwo
