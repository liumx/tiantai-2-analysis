import screen from './screen'
import screentwo from './screentwo'
import agency from './agency'
import visterAnalysis from './visterAnalysis'
import history from './history'
import safePolice from './safePolice'
import safeLaw from './safeLaw'
import safePrice from './safePrice'
import safeMedia from './safeMedia'
import safeAppraise from './safeAppraise'
import forecast from './forecast'
import consume from './consume'


export default {
  screen,
  screentwo,
  agency,
  visterAnalysis,
  history,
  safePolice,
  safeLaw,
  safePrice,
  safeMedia,
  safeAppraise,
  forecast,
  consume
}
