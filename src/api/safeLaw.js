import axios from '@/http'
import {getUrl} from './path'

const law = {
  getTotalLaw (time) {
    return axios({
      method: 'get',
      url: getUrl(`/complain/total/${time}`)
    })
  },
  getAllType (num) {
    return axios({
      method: 'get',
      url: getUrl(`/complain/date?month=${num}`)
    })
  },
  getAnalyze () {
    return axios({
      method: 'get',
      url: getUrl('/distribute/complain')
    })
  }
}

export default law
