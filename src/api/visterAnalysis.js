import axios from '@/http'
import {getUrl} from './path'

const visterAnalysis = {
  getMaleFemaleRatio (date) { // 男女比例
    return axios({
      method: 'get',
      url: getUrl(`/touristsportrait/ratio/malefemale?month=${date}`)
    })
  },
  getAgeRadio (date) {
    return axios({
      method: 'get',
      url: getUrl(`/touristsportrait/ratio/age?month=${date}`)
    })
  },
  getTouristsdisTribution (way, date) {
    return axios({
      method: 'get',
      url: getUrl(`passengerflow/source/${way}?date=${date}`)
    })
  },
  getTripMode (source) {
    return axios({
      method: 'get',
      url: getUrl(`/touristsportrait/ratio/tripmode?month=${source}`)
    })
  },
  likePayWay (source) {
    return axios({
      method: 'get',
      url: getUrl(`/touristsportrait/ratio/consumer?month=${source}`)
    })
  },
  visitorsToTrack (source, time) {
    return axios({
      method: 'get',
      url: getUrl(`/touristsportrait/track/visitor?date=${time}`)
    })
  }
}

export default visterAnalysis
