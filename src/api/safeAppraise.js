import axios from '@/http'
import {getUrl} from './path'

const appraise = {
  getEvaluate () {
    return axios({
      method: 'get',
      url: getUrl('/comment/newest')
    })
  },
  getHotWord () {
    return axios({
      method: 'get',
      url: getUrl(`/frequency/word`)
    })
  },
  getScenicAppraise () {
    return axios({
      method: 'get',
      url: getUrl(`/comment/number`)
    })
  }
}

export default appraise
