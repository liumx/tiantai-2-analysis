// import httpConfig from '@/constant/http-config'
import axios from 'axios'
// import store from './store'

// 此处为axios配置请求头，
axios.interceptors.request.use(
  config => {
    return config
  },
  err => {
    console.log(err)
    return Promise.reject(err)
  }
)
// 响应拦截器即异常处理
axios.interceptors.response.use(
  response => {
    if ((response.status === 200 && response.request.status === 200) || (response.status === 201 && response.request.status === 201)) { // 成功判断
      // if (response.data) {
      console.log(response, '[][][][][][')
      return response.data
    }
    return response
  },
  error => { // 失败判断
    // if (error.response) {
    //   switch (error.response.status){
    //     case 401:
    //       store.dispatch('clearToken').then(() => location.reload())
    //   }
    // }
    return Promise.reject(error.response.data)
  }
)

export default axios
