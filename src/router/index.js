/* eslint-disable object-property-newline */
import Vue from 'vue'
import Router from 'vue-router'

import Analyze from '@/components/Analyze' // 分析主模块
import Channel from '@/components/analyzeContant/Channel' // 这个是渠道模块
import Consume from '@/components/analyzeContant/Consume' // 这个是消费分析模块
import Environment from '@/components/analyzeContant/Environment' // 这个是环境监测模块
import Holiday from '@/components/analyzeContant/Holiday' // 这个是节假日分析模块
import Passenger from '@/components/analyzeContant/Passenger' // 这个是客流检测模块
import Resource from '@/components/analyzeContant/Resource' // 这个是资源查询模块
import Safety from '@/components/analyzeContant/Safety' // 这个是安全检测模块
import Traffic from '@/components/analyzeContant/Traffic' // 这个是交通监控模块
import TravelAgency from '@/components/analyzeContant/TravelAgency' // 这个是旅行社信用分析模块
import Vister from '@/components/analyzeContant/Vister' // 这个是参观者分析模块
import ScreenOne from '@/components/screen'
import ScreenTwo from '@/components/screenTwo'
import Appraise from '@/components/Appraise'

// 客流监测模块
import Forecast from '@/components/passenger/Forecast' // 未来预测
import PassengerFlow from '@/components/passenger/PassengerFlow' // 实时客流
import History from '@/components/passenger/History' // 实时客流

// 安全监控模块
// import AppraiseMonitor from '@/components/safety/AppraiseMonitor' // 评价监测
import MediaMonitor from '@/components/safety/MediaMonitor' // 媒体监测
import PriceMonitor from '@/components/safety/PriceMonitor' // 价格监测
import ComplainLaw from '@/components/safety/ComplainLaw' // 投诉执法
import UrgencyPolice from '@/components/safety/UrgencyPolice' // 应急报警

// 消费分析模块
import AllConsume from '@/components/consume/AllConsume' // 综合统计
import FoodConsumption from '@/components/consume/FoodConsumption' // 综合统计
import HotelConsumption from '@/components/consume/HotelConsumption' // 综合统计
import ScenicConsumption from '@/components/consume/ScenicConsumption' // 综合统计

// 游客画像
import BasicInfo from '@/components/visterAnalysis/BasicInfo' // 综合统计
import Behaviour from '@/components/visterAnalysis/Behaviour' // 综合统计
import VisitorsTrack from '@/components/visterAnalysis/VisitorsTrack' // 综合统计

// 涉旅信用
import Catering from '@/components/TravelAgency/Catering' // 旅行餐饮
import Homestay from '@/components/TravelAgency/Homestay' // 旅行民宿
import TraveCredit from '@/components/TravelAgency/TraveCredit' // 旅行社信用
import TravelHotel from '@/components/TravelAgency/TravelHotel' // 旅行酒店

import Portal from '@/components/Portal'
import Portalone from '@/components/Portalone'
import Portaltwo from '@/components/Portaltwo'
import PortalThree from '@/components/PortalThree'

Vue.use(Router)

const routes = [
  {
    path: '/',
    redirect: '/portalone'
  },
  {
    path: '/analyze',
    name: 'Analyze',
    component: Analyze,
    redirect: '/analyze/consume',
    children: [
      { path: 'channel', name: 'Channel', component: Channel },
      {
        path: 'consume',
        name: 'Consume',
        component: Consume,
        redirect: '/analyze/consume/all-consume',
        children: [
          {path: 'all-consume', name: 'AllConsume', component: AllConsume},
          {path: 'food-consume', name: 'FoodConsumption', component: FoodConsumption},
          {path: 'hotel-consume', name: 'HotelConsumption', component: HotelConsumption},
          {path: 'scenic-consume', name: 'ScenicConsumption', component: ScenicConsumption}
        ]
      },
      { path: 'environment', name: 'Environment', component: Environment },
      { path: 'holiday', name: 'Holiday', component: Holiday },
      {
        path: 'passenger',
        name: 'Passenger',
        component: Passenger,
        redirect: '/analyze/passenger/forecast',
        children: [
          {path: 'forecast', name: 'Forecast', component: Forecast},
          {path: 'passengerflow', name: 'PassengerFlow', component: PassengerFlow},
          {path: 'history', name: 'History', component: History}
        ]
      },
      { path: 'resource', name: 'Resource', component: Resource },
      {
        path: 'safety',
        name: 'Safety',
        component: Safety,
        redirect: '/analyze/safety/media-monitor',
        children: [
          // {path: 'appraise-monitor', name: 'AppraiseMonitor', component: AppraiseMonitor},
          {path: 'media-monitor', name: 'MediaMonitor', component: MediaMonitor},
          {path: 'price-monitor', name: 'PriceMonitor', component: PriceMonitor},
          {path: 'complain-law', name: 'ComplainLaw', component: ComplainLaw},
          {path: 'urgency-police', name: 'UrgencyPolice', component: UrgencyPolice}
        ]
      },
      { path: 'traffic', name: 'Traffic', component: Traffic },
      {
        path: 'travelAgency',
        name: 'TravelAgency',
        redirect: '/analyze/travelAgency/travelHotel',
        component: TravelAgency,
        children: [
          {path: 'catering', name: 'Catering', component: Catering}, // 旅行餐饮
          {path: 'homestay', name: 'Homestay', component: Homestay}, // 旅行民宿
          {path: 'traveCredit', name: 'TraveCredit', component: TraveCredit}, // 旅行社信用
          {path: 'travelHotel', name: 'TravelHotel', component: TravelHotel} // 旅行酒店
        ]
      },
      {
        path: 'vister',
        name: 'Vister',
        component: Vister,
        redirect: '/analyze/vister/basicInfo',
        children: [
          { path: 'basicInfo', name: 'BasicInfo', component: BasicInfo },
          { path: 'behaviour', name: 'Behaviour', component: Behaviour },
          { path: 'visitorsTrack', name: 'VisitorsTrack', component: VisitorsTrack }
        ]
      }
    ]
  },
  { path: '/screenone', name: 'ScreenOne', component: ScreenOne },
  { path: '/screentwo', name: 'ScreenTwo', component: ScreenTwo },
  { path: '/appraise', name: 'Appraise', component: Appraise },
  {path: '/portal', name: 'Portal', component: Portal},
  {path: '/portalone', name: 'Portalone', component: Portalone},
  {path: '/portaltwo', name: 'Portaltwo', component: Portaltwo},
  {path: '/portalThree', name: 'PortalThree', component: PortalThree}
]

const router = new Router({
  mode: 'history',
  routes
})

export default router
